import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @Test
    void ensureNewEmployeeIsCreated() {
        String firstName = "Tino";
        String lastName = "Maria";
        String description = "cleaner";
        String email = "tino@gmail.com";

        Employee employee = new Employee(firstName, lastName, description, email);

        assertNotNull(employee);
    }

    @Test
    void ensureNoFirstNameIsNotCreated() {
        String firstName = "";
        String lastName = "Maria";
        String description = "cleaner";
        String email = "tino@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureNoLastNameIsNotCreated() {
        String firstName = "Tino";
        String lastName = "";
        String description = "cleaner";
        String email = "tino@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureNoDescriptionIsNotCreated() {
        String firstName = "Tino";
        String lastName = "Maria";
        String description = "";
        String email = "tino@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureNoEmailIsNotCreated() {
        String firstName = "Tino";
        String lastName = "Maria";
        String description = "cleaner";
        String email = "";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureInvalidEmailIsNotCreated() {
        String firstName = "Tino";
        String lastName = "Maria";
        String description = "cleaner";
        String email = "asfg.fasf.g";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureInvalidEmailNoDotIsNotCreated() {
        String firstName = "Tino";
        String lastName = "Maria";
        String description = "cleaner";
        String email = "asfg@fasfg";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureFirstNameWithNumbersIsNotCreated() {
        String firstName = "Tino123";
        String lastName = "Maria";
        String description = "cleaner";
        String email = "tino@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureLastNameWithNumbersIsNotCreated() {
        String firstName = "Tino";
        String lastName = "";
        String description = "cleaner";
        String email = "tino@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName, lastName, description, email));
    }

    @Test
    void ensureFirstNameWithSpacesIsCreated() {
        String firstName = "Tino José";
        String lastName = "Maria";
        String description = "cleaner";
        String email = "tino@gmail.com";

        Employee employee = new Employee(firstName, lastName, description, email);

        assertNotNull(employee);
    }

    @Test
    void ensureLastNameWithSpacesIsCreated() {
        String firstName = "Tino José";
        String lastName = "Maria Manel";
        String description = "cleaner";
        String email = "tino@gmail.com";

        Employee employee = new Employee(firstName, lastName, description, email);

        assertNotNull(employee);
    }
}