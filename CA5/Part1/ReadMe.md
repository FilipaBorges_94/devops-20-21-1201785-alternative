# Class Assignment 5 - Part 1

## 1. Analysis, Design and Implementation

This is the README for the first part of fith class assignment.

This class assignment subject is **CI/CD Pipelines with Jenkins**.

## CI/CD Pipelines with Jenkins

In this first part of this class assignment, the goal is to practice with Jenkins using the "gradle basic demo" project used in previous class assignments.

## What is Jenkins

Jenkins is a self-contained, open source automation server which can be used to automate all sorts of tasks related to building, testing, and delivering or deploying software. It supports version control tools such as Git and Mercurial.

### CI/CD Pipeline

CI stands for Continuous Integration and CD stands for Continuous Delivery and Continuous Deployment, and its purpose is to automate the process of the development lifecycle.

### Issues

To this CA we should create issues on Bitbucket to track the development of our application.

During the development of this assigment we should create some issues:

1. Install Jenkins
2. Create a job
3. Create the jenkinsfile
4. Run the job

## Installing Jenkins

Jenkins can be installed using various methods, such as through native system packages, Docker, or even run standalone by any machine with a Java Runtime Environment (JRE) installed.

In this case, we will install Jenkins with the war file that you can download (here)[https://www.jenkins.io/download/].

By this time you should already have de CA5 folder with Part1 inside. Copy the war file inside this folder and open a terminal at this folder and run

```
java -jar jenkins.war
```

![image](Jenkinsrun.png)

After this, the Jenkins application can now be accessed on `localhost:8080`.

You should chose the recommend pluggins and wait until it's finish:

![image](setup.png)

After the initial setup of Jenkins, we can now proceed to create a simple CI/CD pipeline.

## Creating jobs on Jenkins

### Fork the repository to be private

On previous class assignments we neeeded to change the access to our repositories and it's now public. So, to test the jenkins credentials, we will fork our repository and make the fork private.

![image](Fork.png)

### Jenkins Credentials

If a private repository is to be used, we need to give Jenkins the credentials to access and checkout the repository.

On the Jenkins server main page, on the left side, there is an option called `Manage Jenkins`.

After selecting go to `Security` and then `Manage Credentials`, select `global` and on the left side `Add Credentials` and you can now add a new set of credentials.

- Username - Bitbucket username
- Password - Bitbucket password
- ID - Internal ID by which these credentials are identified from jobs and other configurations

![image](Bitbucketcredentials.png)

After adding the necessary credentials, we can now create our first Job.

### Creating Jobs

To create a new job on Jenkins, it's necessary to access the server on `localhost:8080`, and create a new item.

After choosing a name for the job, we need to select the type.

In this case a **Pipeline.**

![image](newpipeline.png)

We now reach the pipeline configuration menu, where we will choose that the pipeline definition will come from a `script from SCM`.

The type of SCM we are using is **Git** so, after selecting it, we need to insert the repository URL and choose the Bitbucket credentials created previously.

The last step is to define the path of the Jenkinsfile, relative to the folder on the repository.

![image](Configurepipeline.png)

## Jenkinsfile

The definition of a Jenkins Pipeline is typically written into a text file (called a Jenkinsfile).It supports two syntaxes, Declarative and Scripted Pipeline. Both of which support building continuous delivery pipelines.

In this Jenkinsfile there are **5 stages**:

- Agent - The agent directive, which is required, instructs Jenkins to allocate an executor and workspace for the Pipeline
- Checkout - This stage will checkout the source code for the project on the defined URL using the credentials with the defined ID (this is the same internal ID defined in Jenkins).
- Assemble - This stage is going to run the Gradle `assemble` task of the project so that it will build its artifacts.
- Test - This stage runs the `test` task of the project, that runs all the unit tests.
- Archiving - This stage archives the build artifacts (for example, distribution zip files or jar files) so that they can be downloaded later. Archived files will be accessible from the Jenkins webpage.

```groovy
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
		git credentialsId: 'fb-bitbucket-credentials', url: 'https://bitbucket.org/FilipaBorges/devops-20-21-1201785_V2/'

            }
        }
        stage('Assemble') {
            steps {
                echo 'Compiling...'
                sh 'cd CA2/Part1/App; ./gradlew assemble'
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
                sh 'cd CA2/Part1/App; ./gradlew test'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'CA2/Part1/App/build/distributions/*'
            }
        }
    }
    post {
        always {
            junit '**/build/test-results/test/*.xml'
        }
    }
}
```

## Running the Jobs

Now that the Jenkinsfile is created and on the root folder of the project, we can now tell Jenkins to run the new CI/CD pipeline. The firsts builds failed because the path was wrong, then the task to run the tests was wrong too and the path to archive was wrong too. But finally we got it:

![image](jenkinsbuild.png)

Now with the post action of publishing test results:

![image](withTestResults.png)

## Tag

With the conclusion of this class assignment task, you can now add the "ca5-part1" tag to your repository.

## References

https://jenkins.io/doc/book/pipeline/jenkinsfile/

https://www.edureka.co/blog/ci-cd-pipeline/
