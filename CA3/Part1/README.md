# Class Assignment 3 - Part 1

## Virtualization with Vagrant

As with the previous assigment, the assigment about virtualization is composed of 2 parts.

In this first part, the focus was a brief introduction to virtualization.

### Virtualization

Virtualization is a technique that allows a computer to emulate other computers so that the software that runs on that emulation, "thinks" that it is running on a separate computer.

#### Hypervisor

A hypervisor is a hardware virtualization technique that allows multiple guest operating systems (OS) to run on a single host system at the same time.

Hypervisors can be divided into two types:

- Type 1: Also known as native or bare-metal hypervisors, these run directly on the host computer’s hardware to control the hardware resources and to manage guest operating systems.

- Type 2: Also known as hosted hypervisors, these run within a formal operating system environment. In this type, the hypervisor runs as a distinct second layer while the operating system runs as a third layer above the hardware.

The chosen hypervisor for the assigment was (VirtualBox)[https://www.virtualbox.org], that is a type 2 hypervisor.

VirtualBox is an OpenSource hypervisor, available for multiple operating systems.

## Issues

To this CA we should create issues on Bitbucket to track the development of our application.

During the development of this assigment we should create 10  issues:

1. Download the VirtualBox;
2. Configure the VM;
3. Run your VM;
4. Prepare git;
5. Clone and execute Spring Tutorial application;
6. Clone your DevOps repository;
7. Install dependencies for previous projects from the repository;
8. Execute the spring boot tutorial basic project;
9. Execute the Gradle basic demo;
10. At the end mark your repository with the tag ca3-part1.

### Configure the Virtual Machine

To be able to access the guest OS, it's necessary to configure the network interface of the machine.
We should connect the (image)[https://help.ubuntu.com/community/Installation/MinimalCD] (ISO) with the Ubuntu 18.04 minimal installation media.
As the several applications that we will run, are going to broadcast to `localhost`, there needs to be a way that the host machine can access the guest.

- The VM need 2048 MB RAM.
- Set Network Adapter 1 as Nat
- Set Network Adapter 2 as Host-only Adapter (vboxnet0).

So, to do that, we need to add to the guest a `Host-only Adapter`. To do that:

- From main menu select File -> Host Network Manager
- Click create button and a new Host-only network will be created and added to the list.

After that we should be able to select a name for your host-only networks in the VMs network configuration. Now, we need to check the IP address range of this network. It should be 192.168.56.1/24.

### Start your VM

![image](VMDone.png)

Now let's hit the start button:

![image](VMRunning.png)

### Preparing git

Now let's continue the setup:

- Update packages repositories
```
sudo apt update
```
- Install the network tools
```
sudo apt install net-tools
```
- Edit the network configuration file to set up the IP
```
sudo nano /etc/netplan/01-netcfg-yaml
```
 should configure something like this:
 ```
 network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
     dhcp4: yes
    enp0s8:
     addresses:
       - 192.168.56.5/24
```
- Apply the new changes:
```
sudo netplan apply
```
- Install openssh-server:
```
sudo apt install openssh-server
```
- Enable password authentication for ssh
```
sudo nano /etc/ssh/sshd_config
```
uncomment the line _PasswordAuthentication yes_
```
sudo service ssh restart
```
- Install an ftp server
```
sudo apt install vsftpd
```
- Enable write access for vsftpd
```
sudo nano /etc/vsftpd.config
```
uncomment the line _write_enable=YES_
```
sudo service vsftpd restart
```

Now we can use our VM from our machine. Just open a new terminal and type:

```
ssh name@192.168.56.5
```

where name is the name of your VM and use the IP of the VM.

Now we only need to download (FileZilla)[https://filezilla-project.org] to use as ftp application to transfer files to/from the VM.

Let's now install git and jdk

```
sudo apt install git
sudo apt install openjdk-8-jdk-headless
```

## Clone and execute Spring Tutorial application

First, make a clone of the application:

```
git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git
```

Then, change to the directory of basic version:

```
cd tut-react-and-spring-data-rest/basic
```

and build and execute the application:

```
./mvnw spring-boot:run
```

You can now see your frontend using the browser in your _host_ computer using the ip of your VM

_http//192.168.56.5:8080/_

![image](SpringRun.png)

## Cloning your repository

Now let's clone our DevOps repository, that we created on the first DevOps class.

First, you should stop the spring boot and then change your working directory:

```
cd
```

This will bring you to the root. Then clone your remote repository into your VM.

```
git clone https://FilipaBorges@bitbucket.org/FilipaBorges/devops-20-21-1201785.git
```

## Running applications

### tut-basic

On the first assignment of the course, a small application of employee information was used to demonstrate how we can work with Git.

Let's now try to run the tut-basic project from CA1 inside our VM:

```
cd devops-20-21-1201785/CA1/tut-basic
```

and then 
```
mvn spring-boot:run
```

What happened?

![image](FailMavenRun.png)

You first need to install maven at your VM!

```
sudo apt install maven
```

Now if you run the previous command again you will have you application running! Don't forget that your VM doesn't have graphic window. To see your application just go to: _http://192.168.56.5:8080_ and there you go!

![image](tut-basicRunning.png)

### gradle_basic_demo

In the first part of the second assignment, a basic multithreaded chat room server application was used.

This VM doesn't have a graphical desktop. As the client part of the application needs to launch a window of the chat room, the VM can only serve as the `server` for the application.

Let's start by installing gradle:

```
sudo apt install gradle
```

Now, let's checkout to the intended application

```
cd devops-20-21-1201785/CA2/Part1/App
``` 

and build the application:

```
gradle build
```

![image](gradleBuild.png)

To launch the server, run the gradle task made to run the server on port `59001`

```
gradle runServer
```

![image](RunServer.png)

With the server running, we can now run the client on the host. Even if we are able to have more than one open terminal, we can't run it on the virtual machine because it doesn't have a graphic window. 

Now, let's open a new terminal in the host. Then, navigate to your repository at CA2 Part1.

If you try to run the task runClient, it will fail, cause it will try to listen to your host address, that doesn't have the server running:

![image](RunClient.png)

To make the client connect through the right IP address, the listening address needs to be changed from localhost to the VM host only adapter. Let's create a new task:

```groovy
task runClientVM(type: JavaExec, dependsOn: classes) {
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
```

and let's run it on our host

```
gradle runClientVM
```

![image](RunClientVM.png)

Perfect! Let's try to add a new client. We need to open a new terminal window and repeat the command.

![image](RunTwoClients.png)

If we close the chat window, the runClientVM will automatically stop running.

![image](closedChat.png)

## Final step

After finalizing the assignment and the README file, the repository should be marked with a new tag 'CA3-part1'.