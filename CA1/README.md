# Class Assignment 1 Report

## Version Control Git

This is the README for the first class assignment regarding Version Control with GIT.

The main goal of this assignment is to implement a simple scenario illustrating a git workflow.

Git is a distributed version control.

To illustrate this scenario it was proposed to add the [Tutorial React.js and Spring Data REST](https://github.com/spring-guides/tut-react-and-spring-data-rest) application to our repository and make changes and improvements to it using the version of the application inside the "basic" folder.

For each improvement or new feature we will have a new branch with the name of the develop feature. For each new feature a new tag should also be created, so we can easily identify new versions of the application on our repository using the tags instead of using the commit id that is automatically generated. 


## 1. Analysis, Design and Implementation

## BitBucket repository

The first step to implement this kind of system is to create a repository.

As proposed by the teachers the repository hosting service chosen to implement was BitBucket, a web-based version control repository by Atlassian.

After creating the repository, which in this case was named ['devops-20-21-1201785'](https://bitbucket.org/FilipaBorges/devops-20-21-1201785/src/master/) , the following step is required to initialize a directory on the local machine that is not on
version control, to be controlled by Git.

We can do this in two ways:

## Turn a local directory into a Git repository:

First it is necessary to go to the project directory that you want to start controlling with Git (you can do it by using the command 
```
cd <path to the intended directory> 
```
or, with the same command, instead of insert manually the path you can simply drag the directory to the command line, and it will assume the path automatically) and then use the following command:

```
$ git init
```

This command creates a new subdirectory named .git, inside your local directory, that contains a Git repository skeleton.

The next step is to connect your local project directory with Bitbucket repository.

```
$ git remote add origin https://linkToRepository/UserName/RepositoryName.git
```

This command will create a new remote called origin at __linkToRepository/UserName/RepositoryName.git__ . Then you should synchronise your local branch master with the remote name origin (now you don't need to type out the whole URL), by using the command:

```
$ git push -u origin master
```

With this command your local master will be sent to the remote repository.

## Clone an existing Git repository:

To clone an existing Git repository you first need to choose in which directory you want to clone the repository. You should use de cd command above, so you can navigate to that directory.

After this, you should execute:

```
git clone https://linkToRepository/UserName/RepositoryName.git
```

This will create a folder named __RepositoryName__ inside the chosen directory. This folder will contain a clone of the bitbucket repository.

You should clone the tut-basic folder that is on the [Tutorial React.js and Spring Data REST](https://github.com/spring-guides/tut-react-and-spring-data-rest) inside a folder named CA1 on your repository. You should also add a README file to report the assignment. 

## Adding the README file

You can add the README file by doing:

```
echo 'This is the read me file' > README
```

inside de CA1 folder and the file will be automatically created with the 'This is the read me file' inside.

## Adding files to your repository

After downloading the application and adding it to the local directory, it's necessary to start version-controlling the files added,
so these files have to be staged using the command:

```
$ git add -A
```

Using the flag __-A__ tell the command to automatically stage all the files that have been modified and deleted, except untracked files. 

You can add just one file changing the __-A__ to the name of the file.

These files are now ready to commit  using the command:

```
$ git commit -m "First commit"
```

A message can be added to the commit using the option __-m__ inserting the message afterwards.

If you want to add and commit files at the same time you should do:

```
$ git commit -a -m "First commit"
```

When all the files that are needed are ready and committed, they can be pushed to the repository using the following command:

```
$ git push
```

At any time you can use 
```
git status
```
to verify in which state you have your files. With this command you can see the new or modified files that aren't in the staging area yet (will appear as "Changes not staged for commit"), and the files on the staging area (it will appear under the "Changes to be committed" heading). After the commit your status will be 'up-to-date'.

## Issues

Creating issues on Bitbucket is useful to track the development of your application.

During the development of this assigment 8 issues were created:

1. Create a new branch named "email-field"
2. Add a new e-mail field
3. Add unit tests for testing the creation of employees and attribute validation
4. Debug server and clients parts
5. Merge email-field branch with master using tag v1.3.0
6. Create new branches for bug fixing
7. Merge the branch to master with tag change in the minor number
8. At the end, mark the repository with tag ca1

## Tagging

When important "checkpoints" are reached in the development of the application, it's fundamental to tag its stage using a
major.minor.revision tag.

The initial version of this assignment was tagged Version 1.2.0

```
$ git tag -a v1.2.0 -m "Initial Commit with Tut-Basic"
```

This command creates a tag object and assigns it to the last commit. The __-a__ flag makes an annotated tag, and the __-m__ flag enables you to add a message to it.

After tagging, it's necessary to push this tag to the repository

```
$ git push origin v1.2.0
```

At this point you should have on your repository the folder CA1 with the folder tut-basic inside, and the README file and associated with this commit the tag v1.2.0.

If you want to tag a particular commit, it can be done by specifying the unique id of that commit.

```
git tag -a v1.2.0 <hash> -m "Initial Commit with Tut-Basic"
```

## Branching

To implement new features to the code, branching should be used to develop over a stable build of the application.

To add the necessary e-mail field to the application, an issue was created, and a branch __email-field__ was added to the repository.

```
$ git branch email-field
```

After the branch is created, it's necessary to checkout from your current branch to the new one using the command:

```
$ git checkout email-field
```

If you want to check your branches, you can run at any time:

```
$  git branch -v
```

or in a graphic way:

```
$  git log --oneline --decorate --graph --all
```

## Adding a email field to the application

In order to implement this new feature, changes to the constructor of the Employee class were made and the addition of getter and setter methods for this field, along with changes on DatabaseLoader and app JavaScript.

Along with this changes, it was necessary to test this new feature.

In order to fulfill this requirement, tests to all attributes of the Employee were made. Now it will not accept null or empty values.

## Merging the new feature branch

After implementing the new feature in the application, and testing it, it is necessary to merge the 'email-field' branch into the 'master' using the following commands:

```
$ git checkout master

$ git merge email-field
```

After merging you should tag:

```
$ git tag -a v1.3.0 -m 'Add email field'
$ git push v1.3.0
```

## Validating email

The next proposed feature to implement in the application is a validator, that makes sure that the inserted email is a valid one (with @ and one dot after).

To implement this feature a new branch was created with the name 'fix-invalid-email'.

```
$ git checkout -b fix-invalid-email
```

This command is an alternative to the previous one, that you used to create the email-field. For the email field you first create the branch and then checkout to move the header to it, but for this command you can checkout to a non existing branch, we will be created automatically.

This feature was implemented creating a validation method 'validateFormat' and change the validateEmail method 
so that it uses the 'validationFormat' method to validate it.

When the fix is implemented and tested, the code should be committed, and the branch merged with 'master'.

```
$ git checkout master

$ git merge fix-invalid-email
```

After this merge a new version of the application is ready, so a tag should be pushed to inform of this.

```
$ git tag -a v1.3.1 -m 'Add email validations'
$ git push v1.3.1
```

## Extra step

To have also a validation to the names beside null or empty, you should create a new branch

```
$ git checkout -b fix-invalid-name
```

This feature was implemented creating a validation method 'checkNameFormat', that will check is the name as no numbers, and change the validateName method 
so that it uses the 'checkNameFormt' method to validate it.

When the fix is implemented and tested, the code should be committed and the branch merged with 'master'.

```
$ git checkout master

$ git merge fix-invalid-name
```

After this merge a new version of the application is ready, so a tag should be pushed to inform of this.

```
$ git tag -a v1.3.2 -m 'Add name validations'
$ git push v1.3.2
```

Small note: since I add a small fix to do with one method name I checked out again to the fix-invalid-name branch and added a tag to the fix v1.3.2.1


## Final step

After finalizing the assignment and the README file, the repository should be market with a new tag 'CA1'.


## 2. Analysis of an Alternative

The chosen alternative was Plastic SCM.

The intended workflow for this alternative is:

task -> branch -> code review -> merge & test -> deploy

The Plastic SCM allows you to either work centralized or distributed. 

Working centralized means that everyone is sharing the server and also shares everyone's work. The central server keeps the history of changes from which everyone requests the latest version of the work and pushes the latest changes to. The big disadvantage is if the central versioning server fails, all will crash, but it's way lighter.

Working distributed means that everyone has a local copy of the entire work’s history. This means that it is not necessary to be online to change revisions or add changes to the work, but tends to be slower.

Now, about Plastic SCM, the major advantages compared to Git:

- Better GUI, even with branch and merge visualization: full traceability and ease of use
- Handle huge files like 4TB
- Super fast, even with big files
- Working on the cloud, you can control the persons that contribute by given permission to certains IP's.
- Allows multiple database backends
- Exclusive check-out

Now, let's talk a little more about branches:

Regarding Git, the branch is just a pointer to a given commit but in Plastic SCM you have real branches, they are changeset containers. This means that all your work regarding that specific bug fix on that branch is only there cause that changes will belong to that only changeset.

About the disadvantages:

- Plastic SCM is not free to use in cloud, team or enterprise editions
- Is not available to Android or iPhone/iPad

## 3. Implementation of the Alternative

The first thing you need to do is download the Plastic SCM Cloud Edition [here](https://www.plasticscm.com/plasticscm-cloud-edition)

After the download, you should install it.

## Configure

After you download it, it will open the first window for you to login, then you should choose Plastic For Developers.

After that you can choose to work centralized or distributed.

If you don't have a local folder yet, you can simply choose your cloud repository and choose to automatically create the folder on your machine. Now you are all set to start working!

![image](WorkspaceExplorer.png)

## Getting started

Now, first thing you should do is copy the tut-basic folder to the new directory that you sync with the cloud.

After copy you should submit the changes. This changes will appear in pending changes:

![image](PendingChanges.png)

## _ignore.conf_ file

Similarly to Git, Plastic SCM has a file here you can add the type of files that you want to wxcluse from version control, like files from "bin" directories. This ignored files can be configured at the _ignore.config_ file.

This file is located at the workspace root path and you can edit and just add the directories you want to ignore inside:

```
bin
obj
```

## Adding files to your repository

Make sure that you are in the intended folder and then run the command 

```
cm add -R *
```

this will add all the new content to version control, that is like the stage area for Git. Then you need to commit this changes:

```
cm ci --all -c "message"
```

the __-c__ flag enables you to write a customized message. At plastic SCM the commit is called checkin.
All your changes will go to the changeset, that will have an automatic number by default. 

Now you just need to push your changesets to your remote cloud:

```
cm push <origin branch> <destination repository>
```

![image](Sync.png)

You can check all the revisions on your changesets by running:

```
cm log
```

![image](ChangeSets.png)

## Labelling

Like you did with tag in Git you should do it with labels at Plastic SCM.

Now that you have the original app version on your repository you should add the label v1.2.0

```
cm label create v1.2.0 -c "Original version of the application"
```

You can check all your labels in the GUI here:

![image](Labels.png)

## Branching

Same thing with the branches, you should do new branches for each new feature. Let's start with the e-mail field branch:

```
cm br create main/email-field
```

now you have the branch, you need to navigate there.
Like you have the Git checkout, here you have switch:

```
cm switch main/email-field 
```

You can check all your branches:

![image](Branches.png)

## New features

The changes in the application will be the same that you did previously. The only difference is about the commands because the procedure will be the same. 

## Merging the new feature branch

First, you need to switch to the main branch, or the one that you want to be the destination of your merge.

Then you should run:

```
cm merge main/email-field --merge 
```

after the merge you need to do add and then checkin the changes from the merge.

## To finish

At this point you should have two new branches (email-field and fix-email-field) already merged into main and all should be functional.

![image](BranchesLine.png)

You can check the changes in the cloud [here](https://www.plasticscm.com/dashboard/cloud/isep_1201785/usage)

