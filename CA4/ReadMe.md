# Class Assignment 4

## 1. Analysis, Design and Implementation

This is the README for the forth class assignment.

This class assignment subject is **Containerization with Docker**.

### Containerization with Docker

In this class assignment, the objective was to setup a containerized environment to execute the gradle version of spring basic tutorial used at class assignment 2 - part 2, with Docker.

The objective is to deploy the application to two containers. The first container will run the web part of the application, to run Tomcat and spring application and the second will be used to execute the H2 server database.

### Containers

So, what is a container?
In a brief summary a container is a standard unit of software that packages up code and all its dependencies, so the application runs quickly and reliably from one computing environment to another.
Containers are an abstraction at the app layer that packages code and dependencies together, sharing the host operating system, being much more lightweight than VM's.

### Docker compose

Compose is a tool for running multi-container Docker applications.
With Compose, you use a `YAML` file to configure your containers, and with a single command control all the services from the configuration.

### Issues

To this CA we should create issues on Bitbucket to track the development of our application.

During the development of this assigment we should create   some issues:

1. Install Docker
2. Starting containers
3. Adding compose file
4. Execute docker-compose up
5. Publish the images to Docker Hub
6. Copy the database file
7. Mark your repository with the tag ca4

### Installing Docker

The first step is to download and install Docker on the machine.
Depending on the operating system, there are different installation methods.

If you're using Mac you will only need the (Docker Desktop)[https://www.docker.com/products/docker-desktop].

After installing the application, we can see if it is running using 

```
docker info
```

![image](dockerInfo.png)

### Starting containers

You can start a container:

```
docker run -d -p 80:80 docker/getting-started
```

This will pull the image of that specific container, and the run the image to start the container. You can check the image by running:

```
docker images
```

![image](dockerImage.png)

You can now delete this image because we will start two new container's user docker-compose.


### Adding compose file

Now, let's copy the demo application from part 2 of the last assignment inside CA4 folder.

After that lets add the _docker-compose.yml_ file and let's edit it:

```
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.33.10
    depends_on:
        - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data
    networks:
      default:
        ipv4_address: 192.168.33.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
```

### Dockerfile

Docker can build images automatically with a Dockerfile. It's a text file that contains all the commands a user can call on the command line to assemble the Docker image.

Some commands are:
 - FROM : Indicates the base image
 - ADD : Add files from host to container
 - COPY : Copies files from host to container
 - CMD : Run a command in terminal
 - ENTRYPOINT : Default application
 - ENV : Sets environment variables
 - EXPOSE : Exposes a port
 - RUN : Run a command in terminal in build fase
 - WORKDIR : Change working directory

The docker build command builds an image from a Dockerfile and a context. The build’s context is the set of files at a specified location PATH or URL. The PATH is a directory on your local filesystem. The URL is a Git repository location.

In order to have more than one Dockerfile, you have to create folders with the name of the container you want to create, in our example, we had to create the db folder and the web, and within each one there is the respective Dockerfile.

### Adding folders

Let's add 3 new folders:
- web
- db
- data

Inside the _web_ folder you should do a Dockerfile where you give instructions to start your container with tomcat and you clone your repository inside it:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://FilipaBorges@bitbucket.org/FilipaBorges/devops-20-21-1201785.git

WORKDIR /tmp/build/devops-20-21-1201785/CA3/Part2/Vagrant/demo

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

Inside de _db_ folder you should also have a Dockerfile to create another container for your app database:

```
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
```

### Compose up

Now that you have everything ready, let's see if we can create the 2 containers. Just move into the folder here you have the _docker-compose_ file and run

```
docker-compose up
```

![image](BuildSuc1.png)
![image](BuildSuc2.png)
![image](DockerImageRunning.png)

Now you have the frontend and database running on docker! You can check your localhost the same way you did for last assignment:

- (Frontend)[http://localhost:8080/demo-0.0.1-SNAPSHOT/]
- (DataBase)[http://localhost:8082/] with the URL _jdbc:h2:tcp://192.168.33.11:9092/./jpadb_

If you run _docker info_ now it will appear all the containers and images:

![image](DockerInfoCompose.png)

### Publish Images at Docker Hub

First you need to create an account at Docker Hub (page)[https://hub.docker.com/].

Then you should create your repository.

After that, you should do the login, so you can then push your images there:

```
docker login
```

Then use your docker ID and your password from Docker Hub registration.

Now let's push the images:

```
docker push [image]:[tag]
```

In this case the __image__ field is the image name, and the __tag__ is normally the version.

In this case, let's first to the tag for the database: 

```
docker tag ca4_db filipaborges/devops-20-21-1201785:db
docker push filipaborges/devops-20-21-1201785:db
```
and do the same for the web.

![image](publish.png)

### Volumes

Volumes are the preferred mechanism for persisting the data generated and used by Docker containers, the volumes are fully managed by Docker. A volume does not increase the size of the containers that use it, and the content of the volume exists outside the life cycle of a given container.

### Copy of the database file

Now let's get a copy of the database file by using the _exec_ to run a shell in the container and copuing the database file to a volume with the db container. First, make sure your containers are running. The open a new shell and run:

```
docker exec ca4_db_1 cp jpadb.mv.db ../data
```

and if you check the folder, you already have the file inside.

![image](Copyfile.png)

The volume was set in docker-compose.yml:

```
  volumes:
      - ./data:/usr/src/data
```

## 2. Analysis of an Alternative

The chosen alternative was (Heroku)[https://www.heroku.com].

Heroku is a free platform as a service based on a managed container system, with integrated data services and a powerful ecosystem, for deploying and running modern apps. 

We can't consider Heroku an alternative to Docker, but a way to deploy docKer containers into the cloud. Heroku allows us to deploy, run and manage applications written in Ruby, Node.js, Java, Python, Clojure, Scala, Go and PHP. 

When the Heroku platform receives the application source, it initiates a build of the source application. In this case, we have a Java application. It will fetch binary library dependencies using Maven, compile the source code together with those libraries, and produce a JAR file to execute. 

The source code of your application, together with the fetched dependencies and output of the build phase such as generated assets or compiled code, as well as the language and framework, are assembled into a slug. These slugs are a fundamental aspect of what happens during application execution - they contain our compiled, assembled application - ready to run - together with the instructions (the Procfile) of what we may want to execute.

Heroku will run your application by running a command you specified in the Procfile, on a dyno that’s been preloaded with your prepared slug.

A running dyno is a lightweight, secure, virtualized Unix container that contains your application slug in its file system. 

Generally, if you deploy an application for the first time, Heroku will run 1 web dyno automatically. In other words, it will boot a dyno, load it with your slug, and execute the command you’ve associated with the web process type in your Procfile.

When you deploy a new version of an application, all the currently executing dynos are killed, and new ones are started to replace them - preserving the existing dyno formation.

What will persist at Heroku are the releases. What is this? The release is combination of slug and configuration. 

Every time you deploy a new version of an application, a new slug is created and release is generated.

A release then, is the mechanism behind how Heroku lets you modify the configuration of your application (the config vars) independently of the application source (stored in the slug) - the release binds them together. Whenever you change a set of config vars associated with your application, a new release will be generated.

## 3. Implementation of the Alternative

The first thing we need to do is signing up (here)[https://signup.heroku.com]. 

### Install 

After register, you should (install)[https://devcenter.heroku.com/articles/getting-started-with-gradle-on-heroku#set-up] Heroku at your machine.

Since I'm using macOs, I can install from Homebrew:

```
sudo chown -R $(whoami) /usr/local/share/man/man8
chmod u+w /usr/local/share/man/man8
brew install heroku/brew/heroku
```

We first changed the ownership of __/usr/local/share/man/man8__ to our user and then we gave write permission to the user, so we can install heroku.

### Login

How that you have installed Heroku you should login:

```
heroku login
```

![image](herokuLogin.png)

### Building Docker Images with heroku.yml

The heroku.yml file is a manifest you can use to define your Heroku app. It allows you to:

- Build Docker images on Heroku
- Specify add-ons and config vars to create during app provisioning
- Take advantage of Review Apps when deploying Docker-based applications

#### First create the Heroku app

First, you need to create an app on Heroku, which prepares Heroku to receive your source code:

```
heroku create
```

![image](Herokuapp.png)

#### Tag your docker image and push it

```
docker tag 591ce884eabd registry.heroku.com/quiet-cliffs-36965/web
docker push registry.heroku.com/quiet-cliffs-36965/web
heroku container:push ca4_web -a quiet-cliffs-36965
```

![image](ContainerPush.png)

You can confirm that this operation was successful at Heroku webpage:

![image](quiet-cliffs.png)

Once the h2 database is not compatible with heroku, we will configure de Add-on and add the resource Heroku Postgres database to your web application.

![image](Database.png)


#### Let's try to run it

Let's see the logs into your Heroku and try to boot your app:

```
heroku logs --tail -a quiet-cliffs-36965
```

We can see that something went wrong:

![image](FailRun.png)

I tried to solve this problem changing the (port)[https://help.heroku.com/LSXQDDSI/why-does-my-java-app-crash-with-an-r10-error] at Config Vars but the error persisted. 

Tried to add the database container, but it failed because it is not supported. I also tried to use the _yml_ file, but it also failed.

#### Deploying your app without container

Once I wasn't able to deploy my docker containers to Heroku, I decided to explore a little more and tried to deploy the app by using WAR deployment.

##### Prepare the project to deploy

The first implementation will use a database in memory in the container deployed in Heroku.

- Procfile:

For heroku to know which file will be executed, you have to create a text file called Procfile without extension that must contain:

```
web java $JAVA_OPTS -jar webapp-runner.jar ${WEBAPP_RUNNER_OPTS} --port $PORT ./build/libs/demo-0.0.1-SNAPSHOT.war
```

- app.js:

You need to change this so that http requests are correctly made:

```
componentDidMount() { // <2>
        client({method: 'GET', path: '/api/employees'}).done(response => {
            this.setState({employees: response.entity._embedded.employees});
        });
    }
```

- application.properties:

Enable the database in memory again:

```
...
spring.datasource.url=jdbc:h2:mem:jpadb
#spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
...
```

##### Create an app

Now we need to create an app at Heroku. I will use one that I already made previously called __nameless-taiga-13358__.

##### Deploy

So let's take care of the build and deploy!

Make sure you log in to Heroku:

```
heroku login
```

Next, let's build the project:

```
./gradlew build
```

To deploy the war file created in the build you have to install the Heroku Java CLI plugin:

```
heroku plugins:install java
```

Now you just need to deploy!

```
heroku war:deploy build/libs/demo-0.0.1-SNAPSHOT.war --app nameless-taiga-13358
```

![image](Deploy.png)

And it's up and running! 

## References

https://www.heroku.com/platform#platform-diagram-detail
https://devcenter.heroku.com/categories/heroku-architecture
https://devcenter.heroku.com/articles/getting-started-with-java#prepare-the-app
https://devcenter.heroku.com/articles/build-docker-images-heroku-yml
