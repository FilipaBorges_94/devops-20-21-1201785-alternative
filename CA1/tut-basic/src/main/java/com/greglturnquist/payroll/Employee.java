/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String email;

    public Employee(String firstName, String lastName, String description, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.email = email;
        validateEmployee(firstName, lastName, description, email);
    }

    public Employee() {
    }

    private void validateEmployee(String firstName, String lastName, String description, String email) {
    	if (!validateName(firstName, lastName) || !validateDescription(description) || !validateEmail(email)){
    		throw new IllegalArgumentException("The data is not valid");
		}
    }

    private boolean validateName(String firstName, String lastName) {
        boolean result = true;
        if (firstName.isEmpty() || lastName.isEmpty() || !checkNameFormat(firstName) || !checkNameFormat(lastName)) {
            result = false;
        }
        return result;
    }

    private boolean validateDescription(String description) {
        boolean result = true;
        if (description.isEmpty() || description == null) {
            result = false;
        }
        return result;
    }

    private boolean validateEmail(String email){
    	boolean result = true;
    	if (email.isEmpty()){
    		result = false;
		}
    	validateFormat();
    	return result;
	}

    private void validateFormat() {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        if (!pattern.matcher(this.email).matches()) {
            throw new IllegalArgumentException("E-mail Address doesn't have a valid format.");
        }
    }

    private boolean checkNameFormat(String name) {
        String alphaRegex = "^[-'a-zA-ZÀ-ÖØ-öø-ÿ\\s]*$"; //with spaces and special chars

        Pattern pat = Pattern.compile(alphaRegex);
        return pat.matcher(name).matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, description);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
// end::code[]
