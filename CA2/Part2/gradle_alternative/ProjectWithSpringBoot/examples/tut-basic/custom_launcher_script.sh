#!/bin/bash

set -e

# Custom Launcher Script for launching a SpringBoot application with 'bazel run'
# this is wired up in the springboot rule (the launcher_script attribute)

echo "USING A CUSTOM LAUNCHER SCRIPT AS A DEMO (see custom_launcher_script.sh)"

# The following environment variables will be set by the springboot rule, and can
# be reliably used for scripting:
#  LABEL_PATH=examples/helloworld
#  SPRINGBOOTJAR_FILENAME=helloworld.jar
#  JVM_FLAGS="-Dcustomprop=gold  -DcustomProp2=silver"
#
# There are several other env variables set by Bazel. These should be stable between
# versions of Bazel because they are documented:
#  https://docs.bazel.build/versions/master/user-manual.html#run

# soon we will use one of the jdk locations already known to Bazel, see Issue #16
if [ -z ${JAVA_HOME} ]; then
  java_cmd="$(which java)"
else
  java_cmd="${JAVA_HOME}/bin/java"
fi

if [ -z "${java_cmd}" ]; then
  echo "ERROR: no java found, either set JAVA_HOME or add the java executable to your PATH"
  exit 1
fi
echo "Using Java at ${java_cmd}"
${java_cmd} -version
echo ""

# java args
echo "Using JAVA_OPTS from the environment: ${JAVA_OPTS}"
echo "Using jvm_flags from the BUILD file: ${JVM_FLAGS}"

# main args
main_args="$@"

# spring boot jar; these are replaced by the springboot starlark code:
path=${LABEL_PATH}
jar=${SPRINGBOOTJAR_FILENAME}

# assemble the command
# use exec so that we can pass signals to the underlying process (https://github.com/salesforce/rules_spring/issues/91)
cmd="exec ${java_cmd} ${JVM_FLAGS} ${JAVA_OPTS} -jar ${path}/${jar} ${main_args}"

echo "Running ${cmd}"
echo "In directory $(pwd)"
echo ""
echo "You can also run from the root of the repo:"
echo "java -jar bazel-bin/${path}/${jar}"
echo ""

${cmd}
