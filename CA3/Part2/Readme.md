# Class Assignment 3 - Part 2

## 1. Analysis, Design and Implementation

This is the README for the third class assignment.

This class assignment subject is **Virtualization with Vagrant**.

## Virtualization with Vagrant

On this second part of the third assignment, the goal is to setup a virtual environment to execute the tutorial spring boot 
application, gradle "basic" version (developed in CA2, Part2).

## Issues

To this CA we should create issues on Bitbucket to track the development of our application.

During the development of this assigment we should create   issues:

1. Install Vagrant
2. Update Vagrantfile
3. Update project with the instructions at https://bitbucket.org/atb/tut-basic-gradle 
4. Start Vagrant
5. Make sure that the frontend is running and your db too
6. Mark your repository with the tag ca3-part2

### Install Vagrant

Go to https://www.vagrantup.com/downloads.html and install the Vagrant version according to your OS.

To check if everything is ok type 
```
vagrant -v
```
in a terminal/console.

![image](VagrantVersion.png)

Create a folder where you want to initialize a vagrant project (for example, CA3/Part2/Vagrant).

Download https://github.com/atb/vagrant-multi-demo to your computer and copy the Vagrantfile to the previously created folder.

## Study the Vagrantfile

The two Virtual Machines we are going to use are:

**web**: this VM is used to run tomcat and the spring boot basic application

**db**: this VM is used to execute the H2 server database

## Update Vagrantfile

Now you should update your Vagrantfile, so it can clone your repository.

![image](VagrantFile.png)

## Update your project

At __build.gradle__ file:

add the plugin:

```
id 'war'
```
and the dependency 
```
providedRuntime 'org.springframework.boot:spring-boot-startet-tomcat'
```

Make a new class __src/main/java/com/greglturnquist/payroll/ServletInitializer.java__

```
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }
}
```

__application.properties__

```
server.servlet.context-path=/basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

__app.js__

```
componentDidMount() { // <2>
		client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```


### Start Vagrant

After the previous steps, to start vagrant you should execute the command:

```
vagrant up
```

(Make sure your in the right folder!)

This command will deploy 2 vm's, one with the web interface of the application, and another with H2 database.

![image](VagrantUp.png)

Note: your repository needs to be public so you can clone it to your VM.

If something goes wrong, like you have the wrong path to the folder, you can make it run the Vagrantfile again in two ways:

- either you kill both VM and do it all over again 

```
vagrant destroy -f
```

and then, after you update de Vagrantfile:

``` 
vagrant up
```

- or you can simply force it to re-run the Vagrantfile

```
vagrant reload --provision
```

### Problem

For some reason the web VM is not recognizing the __org.gradle.wrapper.GradleWrapperMain__ . Let's get in the VM to try so solve the problem.

![image](Error.png)

```
vagrant ssh web
cd devops-20-21-1201785/CA3/Part2/Vagrant/demo
./gradlew clean build
```

The machine displays the same error. After some research I discovered that a _.jar_ file is missing inside the wrapper because of _.gitignore_ file. So, let's get out of your machine, kill it and update your repository:

```
git add -f gradle-wrapper.jar
```

The __-f__ flag forces the file, ignoring that it is inside of your gitignore.

Now let's initialize the VM again and everything will be ok this time! Just run the command:

```
vagrant up
```
and that's it!

## Access your application

Now, if you want to check your frontend you should access:

__http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/__
or
__http://localhost:8080/demo-0.0.1-SNAPSHOT/__

why does the localhost works? Because we mapped the ports on the Vangrantfile.

If you want to check you data base:

__http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console/__
and use URL: __jdbc:h2:tcp://192.168.33.11:9092/./jpadb__
or
__http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console/__

![image](Frontend.png)
![image](H2.png)

Notice that if you shut down your db VM, your frontend will have no data to show, because you changed your application to run with your DB VM.

## Pause your VM

To pause your VM you should do 

```
vagrant halt
```

![image](Halt.png)

You're all set with Vagrant now! 

## 2. Analysis of an Alternative

The chosen alternative for this assignment was Parallels. 

Parallels is a Hypervisor type II, like VirtualBox. The biggest advantages of Parallels are:

- easy to use;
- user interface attractive and intuitive;
- easy to set up;
- has a lot of compatible boxes;
- retina support;
- better quality graphics;
- no configuration needed
- doesn't use any extra battery life when in use

The biggest disadvantages are:

- only free trial, you need to pay license fee;
- some error messages are not user friendly;
- slower than VirtualBox

## 3. Implementation of the Alternative

### Install parallels

First, let's install the (hypervisor)[https://www.parallels.com/eu/products/desktop/welcome-trial/]

![image](ParallelsDesktop.png)

## Configure

You don't need to manually configure the OS that you want to run on your VM, vagrant will do that for you. In order to do that, you need to make sure you have Parallels plugin for vagrant installed. Just type in your terminal:

```
vagrant plugin install vagrant-parallels
```

This will install the latest version of the plugin. But, if your not sure, just run:

```
vagrant plugin update vagrant-parallels
```

![image](ParallelsPlugin.png)

## New Folder

To use this alternative, don't forget to create a new folder into your CA3/Part2 and name it Alternative. Copy the Vagrantfile inside, and the demo folder as well.

## Choosing the box

Once __envimation/ubuntu-xenial__ is only supported by virtualBox, you should decide on another box compatible with Parallels. In this case, I choose __bento/ubuntu-20.04__.

## Changes to Vagrantfile

Now, we need to configure the _Vagrantfile_ with the intended box and change the provider to run Parallels:

```
# See: https://manski.net/2016/09/vagrant-multi-machine-tutorial/
# for information about machine names on private network
Vagrant.configure("2") do |config|
  config.vm.box = "bento/ubuntu-20.04"

# Prefer Parallels before VirtualBox
config.vm.provider "parallels"

  # This provision is common for both VMs
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
  SHELL


  #============
  # Configurations specific to the database VM
  config.vm.define "db" do |db|
    db.vm.box = "bento/ubuntu-20.04"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.33.11"

    # We want to access H2 console from the host using port 8082
    # We want to connet to the H2 server using port 9092
    db.vm.network "forwarded_port", guest: 8082, host: 1234
    db.vm.network "forwarded_port", guest: 9092, host: 1234

    # We need to download H2
    db.vm.provision "shell", inline: <<-SHELL
      wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
    SHELL

    # The following provision shell will run ALWAYS so that we can execute the H2 server process
    # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
    # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
    #
    # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
    db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
    SHELL
  end

  
  #============
  # Configurations specific to the webserver VM
  config.vm.define "web" do |web|
    web.vm.box = "bento/ubuntu-20.04"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.33.10"

    # We set more ram memmory for this VM
    web.vm.provider "parallels" do |v|
      v.memory = 1024
    end

    # We want to access tomcat from the host using port 8086
    web.vm.network "forwarded_port", guest: 8086, host: 8086    

    web.vm.provision "shell", inline: <<-SHELL, privileged: false
      sudo apt-get install git -y
      sudo apt-get install nodejs -y
      sudo apt-get install npm -y
      sudo rm -R /usr/bin/node
      sudo ln -s /usr/bin/nodejs /usr/bin/node
      sudo apt install tomcat8 -y
      sudo apt install tomcat8-admin -y
      # If you want to access Tomcat admin web page do the following:
      # Edit /etc/tomcat8/tomcat-users.xml
      # uncomment tomcat-users and add manager-gui to tomcat user

      # Change the following command to clone your own repository!
       git clone https://FilipaBorges@bitbucket.org/FilipaBorges/devops-20-21-1201785.git
       cd devops-20-21-1201785/CA3/Part2/Alternative/demo
       sudo chmod u+x gradlew
       ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command
      sudo rm -R /var/lib/tomcat8/webapp
      sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    SHELL

  end

end
```

## Gradle Wrapper

Make sure you have the _.jar_ file in your repository. You can look the same command that we used before.

## Install the VM

Now, to install and configure your VM just init the vagrant on the new folder:

```
vagrant up
```

![image](BuildSuccessfull.png)

## Error at TomCat

Something went wrong with TomCat8:

![image](TomCatError.png)

Let's try to install inside the VM

```
vagrant ssh web
sudo apt-get install tomcat9
```

Let's check the tomcat9 folder
```
cd /var/lib
ls
```

![image](/var/lib.png)

We now have the folder, let's deploy the _.war_ file to tomcat9:

```
cd
cd devops-20-21-1201785/CA3/Part2/Alternative/demo
sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat9/webapps
```

Now everything should be running smoothly.

Let's check the frontend and data base:

__http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console/__

__http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/__

Great job!


## References

https://www.winxponmac.com/parallels-vs-virtualbox/
