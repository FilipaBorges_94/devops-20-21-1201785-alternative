# Class Assignment 5 - Part 2

## 1. Analysis, Design and Implementation

This is the README for the second part of fith class assignment.

This class assignment subject is **CI/CD Pipelines with Jenkins**.

## CI/CD Pipelines with Jenkins

In this second part of this class assignment, the goal is to create a pipeline with Jenkins to build the tutorial spring boot application, gradle "basic" version(CA2/Part2).

### Issues

To this CA we should create issues on Bitbucket to track the development of our application.

During the development of this assigment we should create some issues:

1. Create javadoc stage
2. Create DockerFile
3. Create Docker Hub credentials on Jenkins
4. Create the pipeline
5. Run the job
6. Check the container on docker hub

## Create Pipeline

As with the previous part of this assignment, we need to build a CI/CD pipeline to automate the process of the development lifecycle.

We start by creating a new job on our Jenkins server.

First, make sure you go to the folder where you have de _.war_ file and run the command :

```
java -jar jenkins.war
```

Then you can open your localhost and login to your Jenkins.

Let's create a new Item the same way we did for the first part of this CA.

- New Item
- Insert the item name
- Choose pipeline
- Pipeline script from SCM - Git - Repository URL - Same credentials defined on part 1

The _Jenkinsfile_ produced in this second part is built similarly to the first one.
The first part will be the same:

```groovy
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
		git credentialsId: 'fb-bitbucket-credentials', url: 'https://bitbucket.org/FilipaBorges/devops-20-21-1201785_v2/'

            }
        }
        stage('Assemble') {
            steps {
                echo 'Compiling...'
                sh 'cd CA2/Part1/App; ./gradlew assemble'
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
                sh 'cd CA2/Part1/App; ./gradlew test'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'CA2/Part1/App/build/distributions/*'
            }
        }
    }
    post {
        always {
            junit '**/build/test-results/test/*.xml'
        }
    }
}
```

But now we need to add some stages:

- Javadoc: to generate the javadoc of the project and publish it in Jenkins.
- Publish Image: generate a docker image with Tomcat and the war file and publish it in the Docker Hub.

In order to do that we need to install some plugins to our Jenkins. Let's go to our dashboard and choose Manage Jenkins - Manage plugins and then choose:

- Javadoc
- Docker Pipeline

Now that we are all set let's prepare the files.

## Publish the HTML

In this assignment we were asked to publish the HTML files in Jenkins.

To help us add the publishing step to this task, we can use Jenkins **Snippet Generator** to generate the the code.

On the Jenkins main page, select the job created previously and then select **Pipeline Syntax**

Select the **publishHTML: Publish HTML reports** step, and specify the path where the generated HTML files are stored.

![image](SnippetGenerator.png)

After pressing the **Generate Pipeline Script** button, a line of code is generated, that can then be inserted in the Jenkinsfile stage. It's important to add the `target:` that isn't included in the script.

![image](script.png)

So, to generate javadoc with Jenkinsfile we will add this stage:

```
stage('JavaDoc') {
            steps {
                echo 'Generating JavaDoc...'
                sh 'cd CA5/Part2/demo; ./gradlew javadoc'
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: false,
                        reportDir: 'CA5/Part2/demo/build/docs/javadoc',
                        reportFiles: 'index.html',
                        reportName: 'HTML Report',
                        reportTitles: ''
                        ])
            }
        }
```

To help understand this stage:

- allowMissing : if it's true will allow the build to run if the report is missing.
- allwaysLinkToLastBuild and keepAll : if checked, it will keep all past HTML reports, even if build failed.
- reportDir : the path to the HTML report directory relative to the workspace.
- reportFiles : the file to provide links inside the report directory.
- reportName : the name of the report to display for the build/project

## Dockerfile

For Jenkins to generate a docker image we need to use the the Dockerfiles that we learn in the previous class assignment and we need to place it in the same folder as the Jenkinsfile.

For this CA we will use only the web container and we want to initialize it with Tomcat so let's do the Dockerfile:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://FilipaBorges@bitbucket.org/FilipaBorges/devops-20-21-1201785.git

WORKDIR /tmp/build/devops-20-21-1201785/CA5/Part2/demo

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

Once I had problems with the gradlew executable because Jenkins was only finding it on the CA2/Part2 folder and on that application we still were generating the _.jar_ file, I copied the demo folder from CA2/Part2 into CA2/Part2 and made the necessary changes so we can hava a local database and to generate the war file.

Since the problem persisted after that, I just made the gradle build in Jenkins and then, inside the _Dockerfile_ I just copied the generated _.war_ to the tomcat folder. So the final _Dockerfile_ is this:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

ADD build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

We want also to publish the docker image at Docker Hub, so we will create the Docker Hub credentials on our Jenkins, like we did for Bitbucket.

## Jenkinsfile

So let's integrate all of this to the _Jenkinsfile_ :

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
		git credentialsId: 'fb-bitbucket-credentials', url: 'https://bitbucket.org/FilipaBorges/devops-20-21-1201785/'

            }
        }
        stage('Assemble') {
            steps {
                echo 'Compiling...'
                sh 'cd CA5/Part2/demo; ./gradlew assemble'
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
                sh 'cd CA5/Part2/demo; ./gradlew test'
            }
            post {
        	    always {
        	        sh 'cd CA5/Part2/demo/build/test-results/test/; touch *.xml'
                    junit 'CA5/Part2/demo/build/test-results/test/*.xml'
    	        }
            }
        }
	    stage('JavaDoc') {
            steps {
                echo 'Generating JavaDoc...'
                sh 'cd CA5/Part2/demo; ./gradlew javadoc'
                    publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: false,
                        reportDir: 'CA5/Part2/demo/build/docs/javadoc',
                        reportFiles: 'index.html',
                        reportName: 'HTML Report',
                        reportTitles: ''
                        ])
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts '**/build/distributions/*'
            }
        }
	    stage ('Docker Image'){
            steps{
                echo 'Building and pushing Image...'
                sh 'cd CA5/Part2/demo; ./gradlew clean build'
                script{
                    def customImage = docker.build("filipaborges/devops-20-21-1201785:${env.BUILD_NUMBER}", './CA5/Part2/demo')
                    docker.withRegistry( 'https://registry.hub.docker.com', 'fb-dockerhub-credentials') {
                        customImage.push("${env.BUILD_NUMBER}")
                }
            }
        }
    }
}}
```

Now let's run the pipeline!

![image](buildJenkins.png)

Now let's go to Docker Desktop app and try to run this image!

![image](dockerDesktop.png)

Looks like after more than 58 tries we finally got the container up an running smoothly!

## References

https://plugins.jenkins.io/htmlpublisher/

https://www.jenkins.io/doc/book/pipeline/docker/
